package ua.edu.tntu.fis.java.rpisoftware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.tntu.fis.java.rpisoftware.entities.SensorData;
import ua.edu.tntu.fis.java.rpisoftware.services.SensorsService;

@RestController
@RequestMapping("/sensors")
public class SensorsController {
    @Autowired
    private SensorsService sensorsService;

//    @GetMapping(value = "/{sensorid}")
    @GetMapping
    public ResponseEntity<SensorData> getSensorData() {
        SensorData sensorData = sensorsService.getSensorData();
        return ResponseEntity.ok(sensorData);//200
    }

}
