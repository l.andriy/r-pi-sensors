package ua.edu.tntu.fis.java.rpisoftware.entities;

public class SensorData {
    private double temperature;
    private int humidity;

    public SensorData() {
    }

    public SensorData(double temperature) {
        this.temperature = temperature;
    }

    public SensorData(int humidity) {
        this.humidity = humidity;
    }

    public SensorData(double temperature, int humidity) {
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }
}
