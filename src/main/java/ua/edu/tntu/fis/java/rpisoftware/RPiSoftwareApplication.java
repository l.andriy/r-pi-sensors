package ua.edu.tntu.fis.java.rpisoftware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RPiSoftwareApplication {

	public static void main(String[] args) {
		SpringApplication.run(RPiSoftwareApplication.class, args);
	}

}
