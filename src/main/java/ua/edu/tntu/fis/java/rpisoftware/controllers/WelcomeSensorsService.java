package ua.edu.tntu.fis.java.rpisoftware.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.edu.tntu.fis.java.rpisoftware.entities.SensorData;
import ua.edu.tntu.fis.java.rpisoftware.services.SensorsService;

@RestController
@RequestMapping("/")
public class WelcomeSensorsService {

    @GetMapping
    public ResponseEntity<String> welcome() {

        return ResponseEntity.ok("Welcome to our service");//200
    }

}
