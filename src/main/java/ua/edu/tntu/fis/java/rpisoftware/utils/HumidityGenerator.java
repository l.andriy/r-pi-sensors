package ua.edu.tntu.fis.java.rpisoftware.utils;

import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class HumidityGenerator {
    public int generateHumidity() {
        return ThreadLocalRandom.current().nextInt(900);
    }
}
