package ua.edu.tntu.fis.java.rpisoftware.utils;

import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class TemperatureGenerator {
    public double getTemperature() {
        return ThreadLocalRandom.current().nextInt(-10, +30) +
                ThreadLocalRandom.current().nextDouble(); // 0..1
    }
}
