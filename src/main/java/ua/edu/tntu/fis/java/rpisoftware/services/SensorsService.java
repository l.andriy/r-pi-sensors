package ua.edu.tntu.fis.java.rpisoftware.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.edu.tntu.fis.java.rpisoftware.entities.SensorData;
import ua.edu.tntu.fis.java.rpisoftware.utils.HumidityGenerator;
import ua.edu.tntu.fis.java.rpisoftware.utils.TemperatureGenerator;

@Service
public class SensorsService {

    @Autowired
    private TemperatureGenerator temperatureGenerator;

    @Autowired
    private HumidityGenerator humidityGenerator;

    public SensorData getSensorData() {
        return new SensorData(temperatureGenerator.getTemperature(), humidityGenerator.generateHumidity());
    }
}
